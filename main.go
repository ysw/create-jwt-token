package main

import (
	"fmt"
	"freetest/token"
	"io/ioutil"
	"log"
	"time"
)

func main() {
	prvKey, err := ioutil.ReadFile("cert/private.key")
	if err != nil {
		log.Fatalln(err)
	}
	pubKey, err := ioutil.ReadFile("cert/public.key")
	if err != nil {
		log.Fatalln(err)
	}

	jwtToken := token.NewJWT(prvKey, pubKey)

	// 1. Create a new JWT token.
	tok, err := jwtToken.Create(time.Hour, "Can be anything")
	if err != nil {
		log.Fatalln(err)
	}
	fmt.Println("TOKEN:", tok)

	// 2. Validate an existing JWT token.
	content, err := jwtToken.Validate(tok)
	if err != nil {
		log.Fatalln(err)
	}
	fmt.Println("CONTENT:", content)
}
